import { defineStore } from "pinia";
import { useStorage } from "@vueuse/core";

export const useTaskStore = defineStore("task", {
  state: () => ({
    taskList: useStorage("taskList", []),
    id: 0,
  }),
  actions: {
    addTask(item) {
      item.id = this.id++;
      this.taskList.push(item);
    },
    toggleIsDone(id) {
      const task = this.taskList.find((obj) => obj.id === id);
      if (task) {
        task.is_done = !task.is_done;
      }
    },
    deleteTask(itemId) {
      this.taskList = this.taskList.filter((object) => {
        return object.id !== itemId;
      });
    },
  },
});
