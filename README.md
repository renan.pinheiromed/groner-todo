# Groner Todo - Teste Técnico

![](https://img.shields.io/badge/Javascript-grey?style=for-the-badge&logo=javascript)
![](https://img.shields.io/badge/Vue.js-grey?style=for-the-badge&logo=vue.js)
![](https://img.shields.io/badge/Quasar%20Framework-grey?style=for-the-badge&logo=quasar)

> Aplicação PWA para gerenciamento simples de tarefas. Você pode ver, criar, editar e excluir. Todo o gerenciamento de estado da aplicação é feito com Pinia.

## 💻 Pré-requisitos

Antes de começar, verifique se você atendeu aos seguintes requisitos:

- Você instalou a versão mais recente do `Node`.

## 🚀 Instalando Groner Todo

Para instalar o Groner Todo, siga estas etapas:

Na pasta do projeto, execute o seguinte comando:

```bash
npm install
```

## ☕ Usando Groner Todo

Para usar Groner Todo direto no navegador, use o seguinte comando:

```node
quasar dev --mode pwa
```

Para gerar uma build PWA:

```node
quasar build --mode pwa
```

## 📸 Capturas de Tela

### Tela inicial vazia

<img src="public/screenshots/1.png" width="30%" alt="Tela inicial vazia">

### Tela de criação de tarefa

<img src="public/screenshots/2.png" width="30%" alt="Tela de criação de tarefa">

### Tela de edição de tarefa

<img src="public/screenshots/6.png" width="30%" alt="Tela de edição de tarefa">

### Tarefas criadas

<img src="public/screenshots/3.png" width="30%" alt="Tela inicial com 3 tarefas criadas">

### Tarefas concluídas

<img src="public/screenshots/4.png" width="30%" alt="Tela inicial com tarefas concluídas">

## 💡 Tarefas e Fluxo do Projeto

> É possível acessar o link do board do projeto em https://trello.com/b/Dsezxpzw/groner-teste-t%C3%A9cnico.

### Fluxo

- [ ] Abertura de tarefa/demanda e adição ao backlog;
- [ ] Inclusão da demanda na Sprint, baseada em prioridade;
- [ ] Execução da tarefa, respeitando as branches do repositório e se possível, usando GitFlow para ajudar na organização;
- [ ] Após os testes, deve-se fazer a abertura de um Merge Request;
- [ ] Após aprovação, gerar release com todas as alterações, assim concluindo o fluxo de desenvolvimento.
